package com.demo.springboot.rest;





import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;



@Controller
@RequestMapping(value = "/api")
public class DocumentApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentApiController.class);

    @RequestMapping(value = "/document/test{name}", method = RequestMethod.GET)
    public ResponseEntity<Void> testDocument(@PathVariable("name") String name) {
        LOGGER.info("### Dziala metoda testDocument!");
        LOGGER.info("### Potrafie odczytac parametr name: {}",name);

        return new ResponseEntity<>(HttpStatus.OK);
    }

}
